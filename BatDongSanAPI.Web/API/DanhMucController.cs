﻿using AutoMapper;
using BatDongSanAPI.Model.Models;
using BatDongSanAPI.Service;
using BatDongSanAPI.Web.Infrastructure.Core;
using BatDongSanAPI.Web.Mapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BatDongSanAPI.Web.API
{
    [RoutePrefix("api/danhmuc")]
    public class DanhMucController : ApiControllerBase
    {
        IDanhMucService _danhmucService;
        public DanhMucController(IErrorService errorService,IDanhMucService danhmucService):base(errorService)
        {
            this._danhmucService = danhmucService;
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listDanhmuc = _danhmucService.GetAll();
                var listDanhmucVM = Mapper.Map<IEnumerable<DanhMuc>, IEnumerable<DanhMucViewModel>>(listDanhmuc);
                HttpResponseMessage reponse = request.CreateResponse(HttpStatusCode.OK, listDanhmucVM);
                return reponse;
            });
        }
    }
}
