﻿using AutoMapper;
using BatDongSanAPI.Model.Models;
using BatDongSanAPI.Web.Mapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BatDongSanAPI.Web.Mapping
{
    public class AutoMapperConfiguration
    {
       public static void Configure()
        {
            Mapper.CreateMap<DanhMuc, DanhMucViewModel>();
        }
    }
}