﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BatDongSanAPI.Web.Mapping.Models
{
    public class DanhMucViewModel
    {
        public int ID { get; set; }
        public string TenDanhMuc { get; set; }
    }
}