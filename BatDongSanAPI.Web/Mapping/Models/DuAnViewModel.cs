﻿namespace BatDongSanAPI.Web.Mapping.Models
{
    public class DuAnViewModel
    {
        public int ID { get; set; }
        public string TenDuAn { get; set; }
        public string Mota { get; set; }

        public string Anhbia { get; set; }
        public bool HomeFlag { get; set; }
        public int IDTP { get; set; }
    }
}