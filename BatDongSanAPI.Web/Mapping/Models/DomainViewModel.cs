﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BatDongSanAPI.Web.Mapping.Models
{
    public class DomainViewModel
    {
        public int ID { get; set; }
        public string DomainName { get; set; }
    }
}