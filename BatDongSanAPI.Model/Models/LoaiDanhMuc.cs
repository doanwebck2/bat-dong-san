﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("LoaiDanhMucs")]
    public class LoaiDanhMuc
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column(TypeName = "nvarchar")]
        [Required]
        public string TenloaiDM { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string SeoUrl { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string Title { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string Author { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string Keyword { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        public string HinhAnh { get; set; }

        public int IDDanhMuc { get; set; }
        [ForeignKey("IDDanhMuc")]
        public virtual DanhMuc DanhMuc { get; set; }

        public virtual IEnumerable<SanPham> SanPhams { get; set; }
    }
}
