﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BatDongSanAPI.Model.Models
{
    [Table("DuAns")]
    public class DuAn
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column(TypeName = "nvarchar")]
        public string TenDuAn { get; set; }

        [Column(TypeName = "ntext")]
        public string Mota { get; set; }

        public string Anhbia { get; set; }
        public bool HomeFlag { get; set; }
        public int IDTP { get; set; }

        public virtual IEnumerable<SanPham> SanPhams { get; set; }
    }
}