﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("PhuongXas")]
    public class PhuongXa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column(TypeName = "nvarchar")]
        public string TenPhuong { get; set; }
        public int IDQuan { get; set; }

        [ForeignKey("IDQuan")]
        public virtual QuanHuyen QuanHuyen { get; set; }



        public virtual IEnumerable<SanPham> SanPhams { get; set; }
    }
}
