﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("ThanhPhoes")]
    public class ThanhPho
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDThanhPho { get; set; }

        [Required]
        [MaxLength(250)]
        public string TenTP { get; set; }

        public virtual IEnumerable<QuanHuyen> QuanHuyens { get; set; }

    }
}
