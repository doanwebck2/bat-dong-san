﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("SanPhams")]
    public class SanPham
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDSanPham { get; set; }

        public int IDPhanLoaiDM { get; set; }
        public string CreateBy { get; set; }
        public int IDDuAn { get; set; }
        public int IDPhuong { get; set; }
        [Required]
        [MaxLength(250)]
        public string SeoUrl { get; set; }

        [Required]
        public string Title { set; get; }

        [Required]
        public string Description { set; get; }

        [Required]
        public string Author { set; get; }

        [Required]
        public string Keywords { set; get; }

        [Column(TypeName = "xml")]
        [Required]
        public string HinhAnh { get; set; }

        [Required]
        public decimal? Gia { get; set; }

        [Required]
        public DateTime? NgayDangTin { get; set; }

        public DateTime? NgayHetHan { get; set; }

        public float? DienTich { get; set; }

        [MaxLength(250)]
        [Required]
        public string AnhBia { get; set; }

        [MaxLength(20)]
        public string DienThoai { get; set; }

        [MaxLength(250)]
        public string Email { get; set; }

        public float? MatTien { get; set; }

        [MaxLength(250)]
        public string HuongNha { get; set; }

        public int? SoTang { get; set; }

        public int? SoPhongNgu { get; set; }

        public string HuongBanCong { get; set; }
        public int? SoToilet { get; set; }

        [Column(TypeName ="ntext")]
        public string Mota { get; set; }
        public string NoiThat { get; set; }

        [ForeignKey("IDPhanLoaiDM")]
        public virtual LoaiDanhMuc LoaiDanhMuc { get; set; }

        [ForeignKey("IDDuAn")]
        public virtual DuAn DuAn { get; set; }

        [ForeignKey("IDPhuong")]
        public virtual PhuongXa PhuongXa { get; set; }
    }
}
