﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("DanhMucs")]
    public class DanhMuc
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(250)]
        public string TenDanhMuc { get; set; }


        public virtual IEnumerable<LoaiDanhMuc> LoaiDanhMucs { get; set; }
    }
}
