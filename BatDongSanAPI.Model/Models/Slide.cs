﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("Slides")]
    public class Slide
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int ID { set; get; }

        [MaxLength(250)]
        [Required]
        public string HinhAnh { get; set; }

        public string MoTa { get; set; }

        [Required]
        public string Title { get; set; }
    }
}
