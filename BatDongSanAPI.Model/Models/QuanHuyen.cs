﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Model.Models
{
    [Table("QuanHuyens")]
    public class QuanHuyen
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [MaxLength(100)]
        [Required]
        public string TenQuan { get; set; }

        public int IDThanhPho { get; set; }

        [ForeignKey("IDThanhPho")]
        public virtual ThanhPho ThanhPho { get; set; }

        public virtual IEnumerable<PhuongXa> PhuongXas { get; set; }
    }
}
