﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Data.Repositories;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Service
{
    public interface IDanhMucService
    {
        DanhMuc Add(DanhMuc danhmuc);
        void Update(DanhMuc danhmuc);

        DanhMuc Delete(int id);
        IEnumerable<DanhMuc> GetAll();
        void Save();
    }
    public class DanhMucService : IDanhMucService
    {
        private IDanhMucRepository _danhmucRepository;
        private IUnitOfWork _unitOfWork;

        public DanhMucService(IDanhMucRepository danhmucRepository, IUnitOfWork unitOfWork)
        {
            this._danhmucRepository = danhmucRepository;
            this._unitOfWork = unitOfWork;
        }

        public DanhMuc Add(DanhMuc danhmuc)
        {
            return _danhmucRepository.Add(danhmuc);
        }

        public DanhMuc Delete(int id)
        {
            return _danhmucRepository.Delete(id);
        }

        public IEnumerable<DanhMuc> GetAll()
        {
            return _danhmucRepository.GetAll();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DanhMuc danhmuc)
        {
            _danhmucRepository.Update(danhmuc);
        }
    }
}
