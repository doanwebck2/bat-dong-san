﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Data.Repositories;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Service
{
    public interface IErrorService
    {
        Error Create(Error errro);
        void save();
    }
    public class ErrorService : IErrorService
    {
        IErrorRepository _errorRepository;
        IUnitOfWork _unitOfWork;

        public ErrorService(IErrorRepository errorRepository, IUnitOfWork unitOfWork)
        {
            this._errorRepository = errorRepository;
            this._unitOfWork = unitOfWork;
        }
        public Error Create(Error error)
        {
            return _errorRepository.Add(error);
        }

        public void save()
        {
            _unitOfWork.Commit();
        }
    }
}
