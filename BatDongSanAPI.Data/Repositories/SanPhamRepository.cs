﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface ISanPhamRepository : IRepository<SanPham>
    {

    }
    public class SanPhamRepository:RepositoryBase<SanPham>,ISanPhamRepository
    {
        public SanPhamRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
