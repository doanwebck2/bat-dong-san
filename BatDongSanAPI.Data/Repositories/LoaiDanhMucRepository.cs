﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface ILoaiDanhMucRepository : IRepository<LoaiDanhMuc>
    {

    }
    public class LoaiDanhMucRepository : RepositoryBase<LoaiDanhMuc>, ILoaiDanhMucRepository 
    {
        public LoaiDanhMucRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
