﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IDuAnRepository : IRepository<DuAn>
    {

    }


    public class DuAnRepository : RepositoryBase<DuAn>, IDuAnRepository 
    {
        public DuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}