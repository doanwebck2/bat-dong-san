﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IPhuongXaRepository : IRepository<PhuongXa>
    {

    }
    public class PhuongXaRepository:RepositoryBase<PhuongXa>,IPhuongXaRepository
    {
        public PhuongXaRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
