﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IQuanHuyenRepository : IRepository<QuanHuyen>
    {

    }

    public class QuanHuyenRepository:RepositoryBase<QuanHuyen>,IQuanHuyenRepository
    {
        public QuanHuyenRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
