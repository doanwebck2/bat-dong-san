﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IDomainRepository : IRepository<Domain>
    {

    }

    public class DomainRepository : RepositoryBase<Domain>, IDomainRepository
    {
        public DomainRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}