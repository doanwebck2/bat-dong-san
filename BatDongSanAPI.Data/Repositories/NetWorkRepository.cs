﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;

namespace BatDongSanAPI.Data.Repositories
{
    public interface INetWorkRepository : IRepository<Network>
    {

    }


    public class NetWorkRepository:RepositoryBase<Network>,INetWorkRepository
    {
        public NetWorkRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}