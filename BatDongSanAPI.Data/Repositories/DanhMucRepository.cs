﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IDanhMucRepository: IRepository<DanhMuc>
    {

    }
    public class DanhMucRepository:RepositoryBase<DanhMuc>,IDanhMucRepository
    {
        public DanhMucRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
