﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface IThanhPhoRepository:IRepository<ThanhPho>
    {

    }
    public class ThanhPhoRepository:RepositoryBase<ThanhPho>,IThanhPhoRepository
    {
        public ThanhPhoRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
