﻿using BatDongSanAPI.Data.Infrastructure;
using BatDongSanAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatDongSanAPI.Data.Repositories
{
    public interface ISeoHomeRepository : IRepository<SeoHome>
    {

    }
    public class SeoHomeRepository:RepositoryBase<SeoHome>,ISeoHomeRepository
    {
        public SeoHomeRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
