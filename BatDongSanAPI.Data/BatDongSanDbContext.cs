﻿using BatDongSanAPI.Model.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace BatDongSanAPI.Data
{
    public class BatDongSanDbContext : IdentityDbContext<ApplicationUser>
    {
        public BatDongSanDbContext() : base("BatDongSanConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<DanhMuc> DanhMucs { set; get; }
        public DbSet<LoaiDanhMuc> LoaiDanhMucs { set; get; }
        public DbSet<Domain> Domains { set; get; }
        public DbSet<ThanhPho> ThanhPhos { set; get; }
        public DbSet<QuanHuyen> QuanHuyens { set; get; }
        public DbSet<PhuongXa> PhuongXas { set; get; }
        public DbSet<DuAn> DuAns { set; get; }
        public DbSet<SanPham> SanPhams { set; get; }
        public DbSet<SeoHome> SeoHOmes { set; get; }
        public DbSet<Network> NetWorks { set; get; }
        public DbSet<Slide> Slides { set; get; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<PostCategory> PostCategories { set; get; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<SystemConfig> SystemConfigs { get; set; }
        public DbSet<BannerBottom> BannerBottoms { set; get; }
      


        public static BatDongSanDbContext Create()
        {
            return new BatDongSanDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId });
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId);


        }
    }
}