﻿namespace BatDongSanAPI.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private BatDongSanDbContext dbContext;

        public BatDongSanDbContext Init()
        {
            return dbContext ?? (dbContext = new BatDongSanDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}