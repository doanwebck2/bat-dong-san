﻿using System;

namespace BatDongSanAPI.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        BatDongSanDbContext Init();
    }
}