namespace BatDongSanAPI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BannerBottoms",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HinhAnh = c.String(),
                        LienKet = c.String(),
                        Position = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DanhMucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenDanhMuc = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Domains",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DomainName = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DuAns",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenDuAn = c.String(maxLength: 4000),
                        Mota = c.String(storeType: "ntext"),
                        Anhbia = c.String(),
                        HomeFlag = c.Boolean(nullable: false),
                        IDTP = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Errors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        StackTrace = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LoaiDanhMucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenloaiDM = c.String(nullable: false, maxLength: 4000),
                        SeoUrl = c.String(nullable: false, maxLength: 4000),
                        Title = c.String(nullable: false, maxLength: 4000),
                        Description = c.String(nullable: false, maxLength: 4000),
                        Author = c.String(nullable: false, maxLength: 4000),
                        Keyword = c.String(nullable: false, maxLength: 4000),
                        HinhAnh = c.String(nullable: false, maxLength: 4000),
                        IDDanhMuc = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DanhMucs", t => t.IDDanhMuc, cascadeDelete: true)
                .Index(t => t.IDDanhMuc);
            
            CreateTable(
                "dbo.Networks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Icon = c.String(maxLength: 250),
                        Link = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PhuongXas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenPhuong = c.String(maxLength: 4000),
                        IDQuan = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuanHuyens", t => t.IDQuan, cascadeDelete: true)
                .Index(t => t.IDQuan);
            
            CreateTable(
                "dbo.QuanHuyens",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenQuan = c.String(nullable: false, maxLength: 100),
                        IDThanhPho = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ThanhPhoes", t => t.IDThanhPho, cascadeDelete: true)
                .Index(t => t.IDThanhPho);
            
            CreateTable(
                "dbo.ThanhPhoes",
                c => new
                    {
                        IDThanhPho = c.Int(nullable: false, identity: true),
                        TenTP = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.IDThanhPho);
            
            CreateTable(
                "dbo.PostCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Alias = c.String(nullable: false, maxLength: 256, unicode: false),
                        Description = c.String(maxLength: 500),
                        ParentID = c.Int(),
                        DisplayOrder = c.Int(),
                        Image = c.String(maxLength: 256),
                        HomeFlag = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Alias = c.String(nullable: false, maxLength: 256, unicode: false),
                        CategoryID = c.Int(nullable: false),
                        Image = c.String(maxLength: 256),
                        Description = c.String(maxLength: 500),
                        Content = c.String(storeType: "ntext"),
                        HomeFlag = c.Boolean(),
                        HotFlag = c.Boolean(),
                        ViewCount = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PostCategories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.IdentityRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IdentityUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.IdentityRoles", t => t.IdentityRole_Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.IdentityRole_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.SanPhams",
                c => new
                    {
                        IDSanPham = c.Int(nullable: false, identity: true),
                        IDPhanLoaiDM = c.Int(nullable: false),
                        CreateBy = c.String(),
                        IDDuAn = c.Int(nullable: false),
                        IDPhuong = c.Int(nullable: false),
                        SeoUrl = c.String(nullable: false, maxLength: 250),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Author = c.String(nullable: false),
                        Keywords = c.String(nullable: false),
                        HinhAnh = c.String(nullable: false, storeType: "xml"),
                        Gia = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NgayDangTin = c.DateTime(nullable: false),
                        NgayHetHan = c.DateTime(),
                        DienTich = c.Single(),
                        AnhBia = c.String(nullable: false, maxLength: 250),
                        DienThoai = c.String(maxLength: 20),
                        Email = c.String(maxLength: 250),
                        MatTien = c.Single(),
                        HuongNha = c.String(maxLength: 250),
                        SoTang = c.Int(),
                        SoPhongNgu = c.Int(),
                        HuongBanCong = c.String(),
                        SoToilet = c.Int(),
                        Mota = c.String(storeType: "ntext"),
                        NoiThat = c.String(),
                    })
                .PrimaryKey(t => t.IDSanPham)
                .ForeignKey("dbo.DuAns", t => t.IDDuAn, cascadeDelete: true)
                .ForeignKey("dbo.LoaiDanhMucs", t => t.IDPhanLoaiDM, cascadeDelete: true)
                .ForeignKey("dbo.PhuongXas", t => t.IDPhuong, cascadeDelete: true)
                .Index(t => t.IDPhanLoaiDM)
                .Index(t => t.IDDuAn)
                .Index(t => t.IDPhuong);
            
            CreateTable(
                "dbo.SeoHomes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Seo_Title = c.String(),
                        Keywords = c.String(),
                        Author = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Slides",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HinhAnh = c.String(nullable: false, maxLength: 250),
                        MoTa = c.String(),
                        Title = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Systemconfig",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50, unicode: false),
                        ValueString = c.String(maxLength: 50),
                        ValueInt = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(maxLength: 256),
                        Address = c.String(maxLength: 256),
                        Birthday = c.DateTime(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IdentityUserRoles", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserLogins", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserClaims", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.SanPhams", "IDPhuong", "dbo.PhuongXas");
            DropForeignKey("dbo.SanPhams", "IDPhanLoaiDM", "dbo.LoaiDanhMucs");
            DropForeignKey("dbo.SanPhams", "IDDuAn", "dbo.DuAns");
            DropForeignKey("dbo.IdentityUserRoles", "IdentityRole_Id", "dbo.IdentityRoles");
            DropForeignKey("dbo.Posts", "CategoryID", "dbo.PostCategories");
            DropForeignKey("dbo.PhuongXas", "IDQuan", "dbo.QuanHuyens");
            DropForeignKey("dbo.QuanHuyens", "IDThanhPho", "dbo.ThanhPhoes");
            DropForeignKey("dbo.LoaiDanhMucs", "IDDanhMuc", "dbo.DanhMucs");
            DropIndex("dbo.IdentityUserLogins", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserClaims", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.SanPhams", new[] { "IDPhuong" });
            DropIndex("dbo.SanPhams", new[] { "IDDuAn" });
            DropIndex("dbo.SanPhams", new[] { "IDPhanLoaiDM" });
            DropIndex("dbo.IdentityUserRoles", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.Posts", new[] { "CategoryID" });
            DropIndex("dbo.QuanHuyens", new[] { "IDThanhPho" });
            DropIndex("dbo.PhuongXas", new[] { "IDQuan" });
            DropIndex("dbo.LoaiDanhMucs", new[] { "IDDanhMuc" });
            DropTable("dbo.IdentityUserLogins");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.ApplicationUsers");
            DropTable("dbo.Systemconfig");
            DropTable("dbo.Slides");
            DropTable("dbo.SeoHomes");
            DropTable("dbo.SanPhams");
            DropTable("dbo.IdentityUserRoles");
            DropTable("dbo.IdentityRoles");
            DropTable("dbo.Posts");
            DropTable("dbo.PostCategories");
            DropTable("dbo.ThanhPhoes");
            DropTable("dbo.QuanHuyens");
            DropTable("dbo.PhuongXas");
            DropTable("dbo.Networks");
            DropTable("dbo.LoaiDanhMucs");
            DropTable("dbo.Errors");
            DropTable("dbo.DuAns");
            DropTable("dbo.Domains");
            DropTable("dbo.DanhMucs");
            DropTable("dbo.BannerBottoms");
        }
    }
}
