﻿namespace BatDongSanAPI.Data.Migrations
{
    using Model.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BatDongSanAPI.Data.BatDongSanDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BatDongSanAPI.Data.BatDongSanDbContext context)
        {
            CreateDanhMucSample(context);
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

        private void CreateDanhMucSample(BatDongSanAPI.Data.BatDongSanDbContext context)
        {
            if (context.DanhMucs.Count() == 0)
            {
                List<DanhMuc> lstDanhMuc = new List<DanhMuc>()
                {
                    new DanhMuc() {TenDanhMuc="Nhà Đất Bán" },
                    new DanhMuc() {TenDanhMuc="Nhà Đất Cho Thuê" },
                    new DanhMuc() {TenDanhMuc="Dự Án" },
                    new DanhMuc() {TenDanhMuc="Cần Thuê" },
                     new DanhMuc() {TenDanhMuc="Cần Mua" }
                };
                context.DanhMucs.AddRange(lstDanhMuc);
                context.SaveChanges();
            }
        }
    }
}
